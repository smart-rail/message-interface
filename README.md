# Control Tower Message Interface
This project contains the xsd files for the messages to be sent to the control tower. It is for the EU funded Smart Rail project that focuses on information sharing in the logistic sector.

## XSD schemas
The XSD schemas contain the structure validating information for messages to be sent.

## XML examples
Each XSD file has a matching xml file that illustrates a typical message that may be sent.

## Code list
The schemas and example messages contain type codes that are kept separately in a Word document. The reason is that this document may change by adding type codes; the standard itself does not need to change when a type code is added.
